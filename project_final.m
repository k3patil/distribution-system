clc
clear all 
close all


%GIVEN DATA
S=[700 1000 500 1000 1000 1000];
PF=[0.8 0.82 0.85 0.8 0.85 0.8];
L=[0.8 0.85 0.7 0.9 0.85 0.8];
V=[13800 13800 13800 13800 13800 13800];
Z=[0.10 + 0.25i 0.10 + 0.25i 0.12 + 0.35i 0.12 + 0.35i  0.15 + 0.45i 0.15 + 0.45i 0.10 + 0.25i ]; 

Length=[2 3 2.5 4 1.5 3 2];

S=S.*L;
P=S.*PF;
Q=sqrt(S.*S-P.*P); 

%CONVERTING DATA WITH BASES

Sbase= 1000; %kVA
Vbase=13800; %kV
Zbase=(Vbase.*Vbase)/(1000*Sbase);
P=P./Sbase;   
V=V./Vbase;
Q=Q./Sbase;
Z=Z.*(Length./Zbase);

S=complex(P,Q);
Iload=conj(S./V);
Iline=[0 0 0 0 0 0 0];
%---------------------------------Part A-----------------------------------

% Backward Sweep

for i=6:-1:1
    Iline(i)=Iline(i+1)+Iload(i);
   
    
end
% Forward Sweep
for i=1:1:6
    if i==1 
        V(i)= 1- Iline(i).*Z(i);
    else 
        V(i)=V(i-1)-Iline(i).*Z(i);
    end
   
end
Voltage_in_kV= (abs(V).*13.8);
fprintf("Answer of Part-A:\n");
fprintf("in kV:");
disp (Voltage_in_kV)
fprintf("in p.u:");
disp (abs(V))
% PART-A OF FIRST ITTERATION DONE 

%----------------------------------PART-B----------------------------------


inter=0;
error=5;

while error >= (0.01)/(Vbase)
    Vnew=V;
    inter=inter+1;
    
    Iload=conj(S./Vnew);
    

    
for i=6:-1:1
    Iline(i)=Iline(i+1)+Iload(i);
   
    
end

% Forward Sweep
for i=1:6
    if i==1
      
        Vnew= 1- Iline(i).*Z(i);
    else 
        Vnew(i)=Vnew(i-1)-Iline(i).*Z(i);
    end
  
end
error = norm(Vnew - V)/norm(V);
V=Vnew;


end


Distance= [2 5 7.5 11.5 13 16];
fprintf("Answer of Part-B\n ");
fprintf("V in p.u:");
disp (abs(Vnew))
fprintf("Total Number of iterations:");
disp (max(inter)+1) 
Vsec_in_kV= (abs(Vnew).*13.8);
fprintf("V in kV:");
disp (Vsec_in_kV)
subplot(4,2,1);
plot(Distance,abs(Vnew));
grid on 
xlabel ('distance (in mi)');
ylabel ('Voltage (in pu)');
title('Voltage vs Distance')
 
%------------------------------PART C--------------------------------------
Vnew=Vnew.*Vbase;

N=13.8./0.48;
Vs=(Vnew)./N;
Zper=[0.05 0.0575  0.0475  0.0575 0.0575 0.0575];

Vs=Vs-(Vs.*Zper.*L);
Vs_before_tap=Vs;

Tap_Setting=(abs(Vnew)-13800)./(13800);  
Tap_percentage=Tap_Setting.*100;
fprintf("PART-C\n")
fprintf("Calculated Tap Setting :");
disp (Tap_percentage)
fprintf("Voltages before Tap:")
disp (abs(Vs_before_tap)) 
Vi=[0 0 0 0 0 0];
V_W_D=[0 0 0 0 0 0];
    for i=1:2
        Vi(i)=13.8+13.8.*(-0.025);
        Ns=Vi./0.48;
        Vsec=Vnew./Ns ;
        V_W_D=Vsec;
        Vsec=Vsec-Vsec.*Zper.*L;
       
    
    end
  
    for i=3:6 
        Vi(i)= 13.8 + 13.8.*(-0.05);
        Ns=Vi./0.48;
        Vsec=Vnew./Ns;
        V_W_D=Vsec;
        Vsec=Vsec-Vsec.*Zper.*L;
        
    end
fprintf("Selected Tap Setting:");
T_S=[-2.5 -2.5 -5 -5 -5];
disp(T_S)
fprintf("Voltages after Tap");
disp (abs(Vsec))
fprintf("Voltage drops:")
disp (abs(Vsec.*Zper.*L))
fprintf("Voltages without drop");
disp(abs(V_W_D))


%----------------------------Part-D----------------------------------------
Lnew= [2 5 7.5 11.5 13 16 ];
subplot(4,2,2);
plot (Lnew,abs(Vnew))
plot (Lnew,abs(Vs_before_tap))
hold on 
plot (Lnew,abs(Vsec))
xlabel('Distance');
ylabel('Voltage');

%--------------------------PART-E------------------------------------------

Vl=conj(Iline).*Iline.*Z;
Vl=Vl.*Sbase;
fprintf("Part-E answers (Apparent Power Losses)");
disp (abs(Vl))

%-------------------------Part-F-------------------------------------------
%#########################################################################%       
%                     CAPACITOR BANK                                      %
%#########################################################################%
%GIVEN DATA
S=[700 1000 500 1000 1000 1000];
PF=[0.8 0.82 0.85 0.8 0.85 0.8];
L=[0.8 0.85 0.7 0.9 0.85 0.8];
V=[13800 13800 13800 13800 13800 13800];
Z=[0.10 + 0.25i 0.10 + 0.25i 0.12 + 0.35i 0.12 + 0.35i  0.15 + 0.45i 0.15 + 0.45i 0.10 + 0.25i ]; 

Length=[2 3 2.5 4 1.5 3 2];

S=S.*L;
P=S.*PF;
Q=sqrt(S.*S-P.*P); 
Q(5)=Q(5)-750;
%CONVERTING DATA WITH BASES

Sbase= 1000; %kVA
Vbase=13800; %kV
Zbase=(Vbase.*Vbase)/(1000*Sbase);
P=P./Sbase;   
V=V./Vbase;
Q=Q./Sbase;
Z=Z.*(Length./Zbase);

S=complex(P,Q);
Iload=conj(S./V);
Iline=[0 0 0 0 0 0 0];







inter=0;
error=5;

while error >= (0.01)/(Vbase)
    Vnew=V;
    inter=inter+1;
    
    Iload=conj(S./Vnew);
    

    
for i=6:-1:1
    Iline(i)=Iline(i+1)+Iload(i);
   
    
end

for i=1:6
    if i==1
      
        Vnew= 1- Iline(i).*Z(i);
    else 
        Vnew(i)=Vnew(i-1)-Iline(i).*Z(i);
    end
  
end
error = norm(Vnew - V)/norm(V);
V=Vnew;


end


Distance= [2 5 7.5 11.5 13 16];
fprintf("Part-F Answers\n");
fprintf("New voltages in p.u:");
disp (abs(Vnew))
fprintf("Number of iterations:");
disp (max(inter)) 
Vsec_in_kV= (abs(Vnew).*13.8);
fprintf("New Voltages in kV");
disp (Vsec_in_kV)
subplot (4,2,3);
plot (Distance,abs(Vnew));
xlabel ('Distance')
ylabel('Voltage')
 
Vnew=Vnew.*Vbase;

N=13.8./0.48;
Vs=(Vnew)./N;
Zper=[0.05 0.0575  0.0475  0.0575 0.0575 0.0575];

Vs=Vs-(Vs.*Zper.*L);
Vs_before_tap=Vs;

Tap_Setting=(abs(Vnew)-13800)./(13800);  
Tap_percentage=Tap_Setting.*100;
fprintf("Tap setting:");
disp (Tap_percentage)
fprintf("Voltages before Tap:");
disp (abs(Vs_before_tap)) 
Vi=[0 0 0 0 0 0];
for i=1
    Vi(i)=13.8;
        Ns=Vi./0.48;
        Vsec=Vnew./Ns ;
        Vsec=Vsec-Vsec.*Zper.*L;
end

    for i=2
        Vi(i)=13.8+13.8.*(-0.025);
        Ns=Vi./0.48;
        Vsec=Vnew./Ns ;
        Vsec=Vsec-Vsec.*Zper.*L;
       
    
    end
   
    for i=3:6 
        Vi(i)= 13.8 + 13.8.*(-0.05);
        Ns=Vi./0.48;
        Vsec=Vnew./Ns;
        Vsec=Vsec-Vsec.*Zper.*L;
        
    end
T_S_C=[0 -2.5 -5.0 -5.0 -5.0 -5.0];
fprintf("Part-G \n");
fprintf("Selected Tap Setting:");
disp(T_S_C)
fprintf("Voltage after Tap setting");
disp (abs(Vsec))
fprintf("Voltage Drops");
disp (abs(Vsec.*Zper.*L))

Lnew= [2 5 7.5 11.5 13 16 ];
subplot(4,2,4);
plot (Lnew,abs(Vnew))
plot (Lnew,abs(Vs_before_tap))
hold on 
plot (Lnew,abs(Vsec))
xlabel('Distance');
ylabel('Voltage')


Vl=conj(Iline).*Iline.*Z;
Vl=Vl.*Sbase;
fprintf("Answer of Part-H (Apparent power losses)");
disp (abs(Vl))

        
   





